//
//  ViewController.swift
//  website classifier
//
//  Created by Jan Schilliger on 18.09.17.
//  Copyright © 2017 illotros. All rights reserved.
//

import Cocoa
import WebKit

class ViewController: NSViewController {

	let object = webload()
	let siteTitle = NSTextView()
	let siteURL = NSTextView()

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func viewDidAppear() {
		super.viewDidAppear()

		// Create Subview that asks for starting point of the website classification
		

		
		func getString(title: String, question: String, defaultValue: String) -> String {
			let msg = NSAlert()
			msg.addButton(withTitle: "Send")	  // 1st button
			msg.addButton(withTitle: "Default")  // 2nd button
			msg.messageText = title
			msg.informativeText = question
			
			let txt = NSTextField(frame: NSRect(x: 0, y: 0, width: 200, height: 24))
			txt.stringValue = defaultValue
			
			msg.accessoryView = txt
			let response: NSApplication.ModalResponse = msg.runModal()
			
			if (response == NSApplication.ModalResponse.alertFirstButtonReturn) {
				return txt.stringValue
			} else {
				return defaultValue
			}
		}
		
		let stringstartingpoint : String = getString(title: "Startingpoint", question: "Where do you want to start?", defaultValue: String(object.domainsTaken))
		
		let startingpoint = Int64(stringstartingpoint)
		object.domainsTaken = startingpoint!
		
		//load domains from file
		object.initurls()
		//Preload first webview and initialize the webview loading
		object.initloading()
		//Display the first webview
		displayFirsWebView()
		

	}

	override func keyDown(with event: NSEvent) {
		super.keyDown(with: event)

		print(event.keyCode) // 123 126 124
		object.labelcurrent(with: event)
		displayNextWebView()
	}
	
	func displayFirsWebView() {
		
		// Display the first webview
		view.window!.makeFirstResponder(self)

		
		
		//Create the Webview rectangle
		let viewBounds = view.bounds
		let webviewRectangle = NSRect(x: 0, y: 0, width: viewBounds.width, height: viewBounds.height*0.8)
		
		view.addSubview(object.currentwebview!)
		object.currentwebview!.frame = webviewRectangle
		
		// Create the url and site title rectangle
		let urlRectangle = NSRect(x: 0, y: viewBounds.height*0.8, width: viewBounds.width, height: viewBounds.height*0.1)
		let titleRectangle = NSRect(x: 0, y: viewBounds.height*0.9, width: viewBounds.width, height: viewBounds.height*0.1)
		
		view.addSubview(siteTitle)
		view.addSubview(siteURL)
		siteTitle.frame = titleRectangle
		siteURL.frame = urlRectangle
		
		siteURL.isEditable = false
		siteTitle.isEditable = false
		
		// Set the initial texts of the textviews
		siteTitle.string = object.currentwebview!.title!
		siteURL.string = object.currentwebview!.url!.absoluteString
	}

	func displayNextWebView() {
		// First remove the current webview from the views array
		object.currentwebview?.removeFromSuperview()

		object.loadnewcurrent()

		class TaggedWebView: WKWebView {
			override var tag: Int { return 5 }
		}
		let wv = object.currentwebview ?? TaggedWebView()

		if object.currentwebview == nil, let wv = view.viewWithTag(5) {
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
				if wv.superview != nil {
					self.displayNextWebView()
				}
			}
			return
		}
		while let view = view.viewWithTag(5) {
			view.removeFromSuperview()
		}

		if object.currentwebview == nil {
			wv.loadHTMLString("<head><title>Waiting</title><body><h1>Waiting</h1></body>", baseURL: nil)
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
				if wv.superview != nil {
					self.displayNextWebView()
				}
			}
		}

		view.addSubview(wv)

		let viewBounds = view.bounds
		assert(viewBounds.minX == 0)
		assert(viewBounds.minY == 0)
		wv.frame = NSRect(x: 0, y: 0, width: viewBounds.width, height: viewBounds.height*0.8)


		let urlRectangle = NSRect(x: 0, y: viewBounds.height*0.8, width: viewBounds.width, height: viewBounds.height*0.1)
		let titleRectangle = NSRect(x: 0, y: viewBounds.height*0.9, width: viewBounds.width, height: viewBounds.height*0.1)

		siteTitle.frame = titleRectangle
		siteURL.frame = urlRectangle


		siteTitle.string = wv.title ?? "Empty Page"
		siteURL.string = wv.url?.absoluteString ?? "Empty Page"

		view.window!.makeFirstResponder(self)
	}

	override func resignFirstResponder() -> Bool {
		return false
	}

	override var acceptsFirstResponder: Bool {
		return true
	}

	override func viewWillDisappear() {
		// this will be executed, once the program is closed -> save the database
		object.closeprogram()
	}

}

// This class loads the webviews and handles the webvies

class webload : NSObject,WKNavigationDelegate {
	var currentwebview : WKWebView?
	var allwebviews : [WKWebView]?
	var loadingwebviews = [WKWebView]()
	var loadedwebvies = [WKWebView]()
	var count = 1
	var desktopPath : NSString
	var labeldb : SQLiteManager
	let domains: [String]? = nil
	var urls = [URL]()
	var domainsTaken = Int64()
	var undoWebViews = [WKWebView]()

	override init() {
		// Initialize the DB to save the labeled domains
		desktopPath = NSSearchPathForDirectoriesInDomains(.desktopDirectory, .userDomainMask, true).first! as NSString
		labeldb = SQLiteManager(path: desktopPath.appendingPathComponent("SnowHaze/DBs/pornlabel.db"))

		super.init()
		initdb()
	}
	
	func initloading() {
		
		loadfunction()
		// Load the first webview
		let webView = WKWebView(frame: .zero)
		let myURL = urls.removeFirst()
		let myRequest = URLRequest(url: myURL)
		webView.navigationDelegate = self
		webView.load(myRequest)
		currentwebview = webView
	}

	func loadfunction() {
		while loadingwebviews.count + loadedwebvies.count < 100 && loadingwebviews.count < 20 {
			let webView = WKWebView(frame: .zero)
			let myURL = urls.removeFirst()
			let myRequest = URLRequest(url: myURL)
			webView.navigationDelegate = self
			webView.load(myRequest)

			loadingwebviews.append(webView)
		}
	}

	func loadnewcurrent() {
		if loadedwebvies.isEmpty {
			currentwebview = nil
		} else {
			// Perform check, whether loaded webview has been classified before
			currentwebview = loadedwebvies.removeFirst()
			if let _ = currentwebview!.url?.host  {
				let host = SQLite.Data.text(currentwebview!.url!.host!)
				var already_exist = try! labeldb.execute("SELECT EXISTS (SELECT * FROM (SELECT domain FROM nonporn UNION SELECT domain FROM porn UNION SELECT domain FROM revision) WHERE domain = ?)", with: [host])
				if already_exist[0][0]!.boolValue {
					loadnewcurrent()
				}
			}
			
			loadfunction()
		}
	}

	func webView(_ loadedWebview: WKWebView, didFinish: WKNavigation!) {
		loadedwebvies.append(loadedWebview)
		loadingwebviews = loadingwebviews.filter { $0 != loadedWebview }

		// Start loading process
		loadfunction()


	}

	func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
		loadingwebviews = loadingwebviews.filter { $0 != webView}

		// Start loading process
		loadfunction()
	}

	func webView(_ failedwebView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
		loadingwebviews = loadingwebviews.filter { $0 != failedwebView}

		// Start loading process
		loadfunction()

	}

	// This function inserts the labeled domains into the labeled DB
	func labelcurrent(with event:NSEvent) {
		// Test if on the waiting page
		if let rawHost = currentwebview?.url?.host, let _ = currentwebview?.superview {
			let host = SQLite.Data.text(rawHost)
			switch event.keyCode {
				case 123:
					// Insert into Database for non-porn sites
					try! labeldb.execute("INSERT INTO nonporn VALUES (?)", with: [host])
					appendToUndoArray()
				case 126:
					// Insert into Database for porn-sites
					try! labeldb.execute("INSERT INTO porn VALUES (?)", with: [host])
					appendToUndoArray()
				case 124:
					// Insert into Database for sites that need to be revised
					try! labeldb.execute("INSERT INTO revision VALUES (?)", with: [host])
					appendToUndoArray()
				case 125:
			
					// append currentwebview to loadedwebviews
					loadedwebvies.append(currentwebview!)
					
					// Test if undoWebViews exist
					if undoWebViews.count != 0 {
						// remove last webview from undoWebViews and append at first position of loadedwebviews
						let lastWebView = undoWebViews.removeLast()
						loadedwebvies.insert(lastWebView, at: 0)
						let lastHost = SQLite.Data.text(lastWebView.url!.host!)
						
						//Delete host from databases
						try! labeldb.execute("DELETE FROM nonporn WHERE domain = ?", with:[lastHost])
						try! labeldb.execute("DELETE FROM porn WHERE domain = ?", with:[lastHost])
						try! labeldb.execute("DELETE FROM revision WHERE domain = ?", with:[lastHost])
						
					} else {
						// if no undoWebViews exist show text, that it cannot be undone: you're a sucker
						let wv = WKWebView()
						wv.loadHTMLString("<head><title>Sucker</title><body><h1>Cannot Undo operation</h1></body>", baseURL: nil)
						loadedwebvies.insert(wv, at: 0)
					}
				
					//decrease the counter
					domainsTaken -= 2
				
			default:
					print(rawHost)
					print("you are a sucker")
					let wv = WKWebView()
					wv.loadHTMLString("<head><title>Sucker</title><body><h1>Wrong button boy</h1></body>", baseURL: nil)
					currentwebview = wv
					return
			}
			// increase the counter by one
			domainsTaken += 1
		}
	}
	
	func appendToUndoArray() {
		undoWebViews.append(currentwebview!)
		if undoWebViews.count > 10 {
			undoWebViews.removeFirst()
		}
	}

	func initurls() {
		// Read out the domains from the provided list and create the urls array
		desktopPath = NSSearchPathForDirectoriesInDomains(.desktopDirectory, .userDomainMask, true).first! as NSString
		let text = try! String(contentsOfFile: desktopPath.appendingPathComponent("SnowHaze/DBs/2017_07_26.domains"), encoding: .utf8)
		let lines = text.components(separatedBy: "\n")
		for domain in lines.filter({!$0.isEmpty}) {
			urls.append(URL(string: "https://" + domain)!)
			urls.append(URL(string: "http://" + domain)!)
			urls.append(URL(string: "https://www." + domain)!)
			urls.append(URL(string: "http://www." + domain)!)
		}
		urls.removeFirst(Int(domainsTaken))
	}

	func initdb() {
		try! labeldb.execute("PRAGMA locking_mode = EXCLUSIVE")
		try! labeldb.execute("PRAGMA journal_mode = WAL")
		try! labeldb.execute("PRAGMA synchronous = NORMAL")
		try! labeldb.execute("PRAGMA fullfsync = ON")

		if !labeldb.has(table: "porn")! {
			try! labeldb.execute("CREATE TABLE porn (domain TEXT PRIMARY KEY ON CONFLICT IGNORE) WITHOUT ROWID")
		}

		if !labeldb.has(table: "nonporn")! {
			try! labeldb.execute("CREATE TABLE nonporn (domain TEXT PRIMARY KEY ON CONFLICT IGNORE) WITHOUT ROWID")
		}

		if !labeldb.has(table: "revision")! {
			try! labeldb.execute("CREATE TABLE revision (domain TEXT PRIMARY KEY ON CONFLICT IGNORE) WITHOUT ROWID")
		}

		if !labeldb.has(table: "settings")! {
			try! labeldb.execute("CREATE TABLE settings (key TEXT PRIMARY KEY, value WITHOUT ROWID)")
			let host : SQLite.Data = .integer(0)
			try! labeldb.execute("INSERT INTO settings (key,value) VALUES ('offset',?)", with: [host])
		}
		for row in try! labeldb.execute("SELECT value FROM settings WHERE key = 'offset'") {
			domainsTaken = row["value"]!.integer!
		}

	}

	func closeprogram() {

		let host : SQLite.Data = .integer(domainsTaken)
		try! labeldb.execute("REPLACE INTO settings (key,value) VALUES ('offset',?)", with: [host])

		print("porn")
		for row in try! labeldb.execute("SELECT domain FROM porn") { print(row["domain"]!.text!) }

		print("nonporn")
		for row in try! labeldb.execute("SELECT domain FROM nonporn") { print(row["domain"]!.text!) }

		print("revision")
		for row in try! labeldb.execute("SELECT domain FROM revision") { print(row["domain"]!.text!) }

		print("settings")
		for row in try! labeldb.execute("SELECT value FROM settings WHERE key = 'offset'") { print(row["value"]!.integer!) }

		// save the number of domains taken from the database
		// close the database with the labeled domains
		// close the database with domains to be checked
		try! labeldb.execute("PRAGMA wal_checkpoint(TRUNCATE)")
	}
}



